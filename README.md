# 手把手教你0基础实战SpringBoot+Vue前后端分离博客系统

从头开始带你手写一套完整的前后端分离的博客系统，无套路，全程写代码 + 讲解知识点，全程干货

 

## 微信公众号：毕设小帮手

关注公众号，回复【软件】，获取项目配套软件~


## B站视频（程序员瑞哥）

https://space.bilibili.com/489083760

### B站直播间

https://live.bilibili.com/26965598



### 账号密码

管理员账号密码：程序员瑞哥/adminadmin

普通用户账号密码：testtest/adminadmin



## 项目效果图展示



### 登录注册



![img](README.assets/1691389363289-b6290587-3eeb-4e3e-8600-160399eeca16.png)

![img](README.assets/1691501305886-96557abf-228f-461f-88df-a7a7c54f4839.png)



### 前台页面

![img](README.assets/1691501335231-160d8d00-cc74-4a80-a0b5-e241ddb5d519.png)





![img](README.assets/1691501350128-9873f18a-a861-4340-90bc-2d15aed8f1f8.png)



#### 管理员：

![img](README.assets/1691501158205-c906cd93-08a2-41bf-94d0-ab75f85a56d8.png)



![img](README.assets/1691501179160-5e67b3c6-afb4-48c1-9469-29d286478741.png)



![img](README.assets/1691501193165-a35daa99-dfc6-457e-96cc-f972ea92cb6c.png)

![img](README.assets/1691501204455-b09d4ff7-feae-4394-98b2-591d4d3663d4.png)



![img](README.assets/1691501217934-8ca58d08-5c07-489f-af1f-e08eca4a19d3.png)



![img](README.assets/1690355938261-d95eb58b-0727-44bb-81f1-45bfe0d1f704.png)



普通用户页面：

![img](README.assets/1691501285085-a51345ad-c6ac-4164-9ad2-d01a16d3a490.png)



## 功能清单

- 登录注册

- 基于RBAC的权限模型（可分配角色和菜单权限）

- 单表增删改查（包括分页模糊查询）

- 文件上传

- 服务器部署

- 富文本编辑器

- JWT权限认证

- Vue组件化

- Vue混入使用

- 批量删除

- 个人信息编辑

  



### 项目简介：

【1、0基础实战SpringBoot+Vue前后端分离毕业设计项目介绍】

视频：  https://www.bilibili.com/video/BV1Lo4y1Y7Bi/?share_source=copy_web&vd_source=3decf3905edf4544cc498962e3f4b5df



【2 、学会本项目的收获】

视频： https://www.bilibili.com/video/BV15T411i7p4/?share_source=copy_web&vd_source=3decf3905edf4544cc498962e3f4b5df



### 系统技术 启动环境介绍：

**后端技术栈：**

- **jdk 1.8**
- **mysql5.7+**

**前端技术栈：**

- **Node v16.2.0**
- Vue2.x
- Element-UI

**开发过程中使用的软件：**

- IDEA2022.2
- **PHPStudy**
- Navicat
- Vscode
- PostMan













