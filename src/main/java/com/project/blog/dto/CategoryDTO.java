package com.project.blog.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 栏目表
 * </p>
 *
 * @author B站：程序员瑞哥
 * @since 2023-03-03
 */
@Data
public class CategoryDTO extends PageInfo {

    @ApiModelProperty("标题")
    private String title;

}
