package com.project.blog.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 文章表
 * </p>
 *
 * @author B站：程序员瑞哥
 * @since 2023-03-03
 */
@Data
public class ArticleDTO extends PageInfo {

    @ApiModelProperty("标题")
    private String title;

}
