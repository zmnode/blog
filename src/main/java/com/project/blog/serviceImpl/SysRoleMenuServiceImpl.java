package com.project.blog.serviceImpl;

import com.project.blog.entity.SysRoleMenu;
import com.project.blog.mapper.SysRoleMenuMapper;
import com.project.blog.service.SysRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色菜单关系表 服务实现类
 * </p>
 *
 * @author B站：程序员瑞哥
 * @since 2023-03-24
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {

}
