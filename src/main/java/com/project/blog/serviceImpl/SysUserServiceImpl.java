package com.project.blog.serviceImpl;

import com.project.blog.entity.SysUser;
import com.project.blog.mapper.SysUserMapper;
import com.project.blog.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author B站：程序员瑞哥
 * @since 2023-02-25
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

}
