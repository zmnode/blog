package com.project.blog.serviceImpl;

import com.project.blog.entity.SysMenu;
import com.project.blog.mapper.SysMenuMapper;
import com.project.blog.service.SysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author B站：程序员瑞哥
 * @since 2023-03-23
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

}
