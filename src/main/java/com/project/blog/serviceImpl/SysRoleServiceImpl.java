package com.project.blog.serviceImpl;

import com.project.blog.entity.SysRole;
import com.project.blog.mapper.SysRoleMapper;
import com.project.blog.service.SysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author B站：程序员瑞哥
 * @since 2023-03-23
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

}
