package com.project.blog.service;

import com.project.blog.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author B站：程序员瑞哥
 * @since 2023-02-25
 */
public interface SysUserService extends IService<SysUser> {

}
