package com.project.blog.service;

import com.project.blog.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author B站：程序员瑞哥
 * @since 2023-03-23
 */
public interface SysRoleService extends IService<SysRole> {

}
