package com.project.blog.service;

import com.project.blog.entity.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色菜单关系表 服务类
 * </p>
 *
 * @author B站：程序员瑞哥
 * @since 2023-03-24
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

}
