package com.project.blog.service;

import com.project.blog.entity.Article;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 文章表 服务类
 * </p>
 *
 * @author B站：程序员瑞哥
 * @since 2023-03-04
 */
public interface ArticleService extends IService<Article> {

}
