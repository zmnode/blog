package com.project.blog.service;

import com.project.blog.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 栏目表 服务类
 * </p>
 *
 * @author B站：程序员瑞哥
 * @since 2023-03-03
 */
public interface CategoryService extends IService<Category> {

}
