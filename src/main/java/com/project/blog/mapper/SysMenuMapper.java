package com.project.blog.mapper;

import com.project.blog.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author B站：程序员瑞哥
 * @since 2023-03-23
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
