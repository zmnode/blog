import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '../views/Layout.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Layout,
    children:[
      {
        path:"/",
        name:"",
        component: ()=> import('../views/admin/Home.vue')
      },
      {
        path:"/test",
        name:"Test",
        component: ()=> import('../views/admin/Test')
      },
      {
        path:"/test2",
        name:"Test2",
        component: ()=> import('../views/admin/Test2')
      },
      {
        path:"/categoryManage",
        name:"CategoryManage",
        component: ()=> import('../views/admin/categoryManage')
      },
      {
        path:"/articleManage",
        name:"ArticleManage",
        component: ()=> import('../views/admin/articleManage')
      },
      {
        path:"/sysUserManage",
        name:"SysUserManage",
        component: ()=> import('../views/admin/sysUserManage')
      },
      {
        path:"/sysRoleManage",
        name:"SysRoleManage",
        component: ()=> import('../views/admin/sysRoleManage')
      },
      {
        path:"/sysMenuManage",
        name:"SysMenuManage",
        component: ()=> import('../views/admin/sysMenuManage')
      },
      {
        path: '/editUserInfo',
        name: 'EditUserInfo',
        component: () => import('../views/admin/editUserInfo.vue')
      },
    ]
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path:"/login",
    name:"Login",
    component: () => import('../views/Login')
  },
  {
    path:"/register",
    name:"Register",
    component: () => import('../views/Register')
  },
  {
    path:"/article",
    name:"Article",
    component: () => import('../views/front/Article')
  },
  {
    path:"/detail",
    name:"Detail",
    component: () => import('../views/front/Detail')
  },
]

const router = new VueRouter({
  //路由模式
  // mode: 'history',
  routes
})

export default router
