/*
 Navicat Premium Data Transfer

 Source Server         : root@localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : blog

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 07/08/2023 14:12:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '文章内容',
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '封面图',
  `category_id` int(2) NULL DEFAULT NULL COMMENT '所属栏目',
  `create_user_id` int(11) NULL DEFAULT NULL COMMENT '发布用户',
  `views` int(11) NULL DEFAULT 0 COMMENT '浏览量',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `status` int(1) NULL DEFAULT 1 COMMENT '状态/已发布/取消发布',
  `update_user_id` int(11) NULL DEFAULT NULL COMMENT '更新用户',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '文章表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES (1, 'HTML5事件—visibilitychange页面可见性事件监听', '<p><span style=\"color: rgb(102, 102, 102); background-color: rgb(246, 246, 246); font-size: 14px;\">无意中注意到的这个有意思的api，visibilitychange事件是浏览器新添加的一个事件，当浏览器的某个标签页切换到后台，或从后台切换到前台时就会触发该消息，现在主流的浏览器都支持该消息了，例如Chrome, Firefox, IE10等。虽然这只是一个简单的功能，但这对于采用HTML5开发移动端产品来说是天赐福音，比如用户正在玩游戏时，突然切换到后台去发一条短信或打一个电话，再切换到游戏，那么开发者就需要捕捉对这些突发情形进行处理，当游戏切换到后台时就暂停游戏，从后台切换回来时，又能允许用户继续游戏</span></p><p><br></p><h2 style=\"text-align: start;\"><strong>基本用法</strong></h2><p><span style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 14px;\">通过</span><span style=\"color: rgb(221, 17, 68); background-color: rgb(249, 242, 244);\">document.visibilityState</span><span style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 14px;\">（返回visible | hidden）或者</span><span style=\"color: rgb(221, 17, 68); background-color: rgb(249, 242, 244);\">document.hidden</span><span style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 14px;\">（返回true | false，true为不可见）可以判断当前文档是否可见；提示：w3c官方更推荐使用前者document.visibilityState方式</span></p><p><br></p><pre><code class=\"language-javascript\">&lt;!DOCTYPE html&gt;\n&lt;html lang=\"en\"&gt;\n\n&lt;head&gt;\n  &lt;meta charset=\"UTF-8\"&gt;\n  &lt;meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"&gt;\n  &lt;meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"&gt;\n  &lt;title&gt;Document&lt;/title&gt;\n&lt;/head&gt;\n\n&lt;body&gt;\n\n&lt;/body&gt;\n&lt;script&gt;\n  document.addEventListener(\"visibilitychange\", function () {\n    // 判断方式一\n    if (document.visibilityState == \"hidden\") {\n      console.log(\'用户离开了\')\n    }else {\n      console.log(\'用户回来了\')\n    }\n   \n    // 判断方式二\n    if(document.hidden){\n      console.log(\'用户离开了\')\n    }\n  });\n&lt;/script&gt;\n\n&lt;/html&gt;</code></pre><p><br></p>', 'http://localhost:9999/views?fileName=c25dda15-b304-48f1-be2e-9d465bcb5d48.jpg', 1, 1, 25, '2023-02-21 11:12:50', '2023-04-06 09:03:40', 0, 1);
INSERT INTO `article` VALUES (2, 'HTML5事件—visibilitychange页面可见性事件监听', '<p><span style=\"color: rgb(102, 102, 102); background-color: rgb(246, 246, 246); font-size: 14px;\">无意中注意到的这个有意思的api，visibilitychange事件是浏览器新添加的一个事件，当浏览器的某个标签页切换到后台，或从后台切换到前台时就会触发该消息，现在主流的浏览器都支持该消息了，例如Chrome, Firefox, IE10等。虽然这只是一个简单的功能，但这对于采用HTML5开发移动端产品来说是天赐福音，比如用户正在玩游戏时，突然切换到后台去发一条短信或打一个电话，再切换到游戏，那么开发者就需要捕捉对这些突发情形进行处理，当游戏切换到后台时就暂停游戏，从后台切换回来时，又能允许用户继续游戏</span></p><p><br></p><h2 style=\"text-align: start;\"><strong>基本用法</strong></h2><p><span style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 14px;\">通过</span><span style=\"color: rgb(221, 17, 68); background-color: rgb(249, 242, 244);\">document.visibilityState</span><span style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 14px;\">（返回visible | hidden）或者</span><span style=\"color: rgb(221, 17, 68); background-color: rgb(249, 242, 244);\">document.hidden</span><span style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 14px;\">（返回true | false，true为不可见）可以判断当前文档是否可见；提示：w3c官方更推荐使用前者document.visibilityState方式</span></p><p><br></p><pre><code class=\"language-javascript\">&lt;!DOCTYPE html&gt;\n&lt;html lang=\"en\"&gt;\n\n&lt;head&gt;\n  &lt;meta charset=\"UTF-8\"&gt;\n  &lt;meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"&gt;\n  &lt;meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"&gt;\n  &lt;title&gt;Document&lt;/title&gt;\n&lt;/head&gt;\n\n&lt;body&gt;\n\n&lt;/body&gt;\n&lt;script&gt;\n  document.addEventListener(\"visibilitychange\", function () {\n    // 判断方式一\n    if (document.visibilityState == \"hidden\") {\n      console.log(\'用户离开了\')\n    }else {\n      console.log(\'用户回来了\')\n    }\n   \n    // 判断方式二\n    if(document.hidden){\n      console.log(\'用户离开了\')\n    }\n  });\n&lt;/script&gt;\n\n&lt;/html&gt;</code></pre><p><br></p>', NULL, 1, 1, 16, '2023-02-21 11:12:50', '2023-02-24 07:46:34', 1, NULL);
INSERT INTO `article` VALUES (3, 'HTML5事件—visibilitychange页面可见性事件监听', '<p><span style=\"color: rgb(102, 102, 102); background-color: rgb(246, 246, 246); font-size: 14px;\">无意中注意到的这个有意思的api，visibilitychange事件是浏览器新添加的一个事件，当浏览器的某个标签页切换到后台，或从后台切换到前台时就会触发该消息，现在主流的浏览器都支持该消息了，例如Chrome, Firefox, IE10等。虽然这只是一个简单的功能，但这对于采用HTML5开发移动端产品来说是天赐福音，比如用户正在玩游戏时，突然切换到后台去发一条短信或打一个电话，再切换到游戏，那么开发者就需要捕捉对这些突发情形进行处理，当游戏切换到后台时就暂停游戏，从后台切换回来时，又能允许用户继续游戏</span></p><p><br></p><h2 style=\"text-align: start;\"><strong>基本用法</strong></h2><p><span style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 14px;\">通过</span><span style=\"color: rgb(221, 17, 68); background-color: rgb(249, 242, 244);\">document.visibilityState</span><span style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 14px;\">（返回visible | hidden）或者</span><span style=\"color: rgb(221, 17, 68); background-color: rgb(249, 242, 244);\">document.hidden</span><span style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 14px;\">（返回true | false，true为不可见）可以判断当前文档是否可见；提示：w3c官方更推荐使用前者document.visibilityState方式</span></p><p><br></p><pre><code class=\"language-javascript\">&lt;!DOCTYPE html&gt;\n&lt;html lang=\"en\"&gt;\n\n&lt;head&gt;\n  &lt;meta charset=\"UTF-8\"&gt;\n  &lt;meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"&gt;\n  &lt;meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"&gt;\n  &lt;title&gt;Document&lt;/title&gt;\n&lt;/head&gt;\n\n&lt;body&gt;\n\n&lt;/body&gt;\n&lt;script&gt;\n  document.addEventListener(\"visibilitychange\", function () {\n    // 判断方式一\n    if (document.visibilityState == \"hidden\") {\n      console.log(\'用户离开了\')\n    }else {\n      console.log(\'用户回来了\')\n    }\n   \n    // 判断方式二\n    if(document.hidden){\n      console.log(\'用户离开了\')\n    }\n  });\n&lt;/script&gt;\n\n&lt;/html&gt;</code></pre><p><br></p>', 'http://localhost:9999/views?fileName=6224018d-cb0d-4ae1-86f3-5b3188854b37.png', 1, 1, 16, '2023-02-21 11:12:50', '2023-04-05 16:23:01', 1, NULL);
INSERT INTO `article` VALUES (4, 'HTML5事件—visibilitychange页面可见性事件监听', '<p><span style=\"color: rgb(102, 102, 102); background-color: rgb(246, 246, 246); font-size: 14px;\">无意中注意到的这个有意思的api，visibilitychange事件是浏览器新添加的一个事件，当浏览器的某个标签页切换到后台，或从后台切换到前台时就会触发该消息，现在主流的浏览器都支持该消息了，例如Chrome, Firefox, IE10等。虽然这只是一个简单的功能，但这对于采用HTML5开发移动端产品来说是天赐福音，比如用户正在玩游戏时，突然切换到后台去发一条短信或打一个电话，再切换到游戏，那么开发者就需要捕捉对这些突发情形进行处理，当游戏切换到后台时就暂停游戏，从后台切换回来时，又能允许用户继续游戏</span></p><p><br></p><h2 style=\"text-align: start;\"><strong>基本用法</strong></h2><p><span style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 14px;\">通过</span><span style=\"color: rgb(221, 17, 68); background-color: rgb(249, 242, 244);\">document.visibilityState</span><span style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 14px;\">（返回visible | hidden）或者</span><span style=\"color: rgb(221, 17, 68); background-color: rgb(249, 242, 244);\">document.hidden</span><span style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 14px;\">（返回true | false，true为不可见）可以判断当前文档是否可见；提示：w3c官方更推荐使用前者document.visibilityState方式</span></p><p><br></p><pre><code class=\"language-javascript\">&lt;!DOCTYPE html&gt;\n&lt;html lang=\"en\"&gt;\n\n&lt;head&gt;\n  &lt;meta charset=\"UTF-8\"&gt;\n  &lt;meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"&gt;\n  &lt;meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"&gt;\n  &lt;title&gt;Document&lt;/title&gt;\n&lt;/head&gt;\n\n&lt;body&gt;\n\n&lt;/body&gt;\n&lt;script&gt;\n  document.addEventListener(\"visibilitychange\", function () {\n    // 判断方式一\n    if (document.visibilityState == \"hidden\") {\n      console.log(\'用户离开了\')\n    }else {\n      console.log(\'用户回来了\')\n    }\n   \n    // 判断方式二\n    if(document.hidden){\n      console.log(\'用户离开了\')\n    }\n  });\n&lt;/script&gt;\n\n&lt;/html&gt;</code></pre><p><br></p>', NULL, 1, 1, 18, '2023-02-21 11:12:50', '2023-04-05 17:24:39', 1, NULL);
INSERT INTO `article` VALUES (5, 'HTML5事件—visibilitychange页面可见性事件监听', '<p><span style=\"color: rgb(102, 102, 102); background-color: rgb(246, 246, 246); font-size: 14px;\">无意中注意到的这个有意思的api，visibilitychange事件是浏览器新添加的一个事件，当浏览器的某个标签页切换到后台，或从后台切换到前台时就会触发该消息，现在主流的浏览器都支持该消息了，例如Chrome, Firefox, IE10等。虽然这只是一个简单的功能，但这对于采用HTML5开发移动端产品来说是天赐福音，比如用户正在玩游戏时，突然切换到后台去发一条短信或打一个电话，再切换到游戏，那么开发者就需要捕捉对这些突发情形进行处理，当游戏切换到后台时就暂停游戏，从后台切换回来时，又能允许用户继续游戏</span></p><p><br></p><h2 style=\"text-align: start;\"><strong>基本用法</strong></h2><p><span style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 14px;\">通过</span><span style=\"color: rgb(221, 17, 68); background-color: rgb(249, 242, 244);\">document.visibilityState</span><span style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 14px;\">（返回visible | hidden）或者</span><span style=\"color: rgb(221, 17, 68); background-color: rgb(249, 242, 244);\">document.hidden</span><span style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 14px;\">（返回true | false，true为不可见）可以判断当前文档是否可见；提示：w3c官方更推荐使用前者document.visibilityState方式</span></p><p><br></p><pre><code class=\"language-javascript\">&lt;!DOCTYPE html&gt;\n&lt;html lang=\"en\"&gt;\n\n&lt;head&gt;\n  &lt;meta charset=\"UTF-8\"&gt;\n  &lt;meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"&gt;\n  &lt;meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"&gt;\n  &lt;title&gt;Document&lt;/title&gt;\n&lt;/head&gt;\n\n&lt;body&gt;\n\n&lt;/body&gt;\n&lt;script&gt;\n  document.addEventListener(\"visibilitychange\", function () {\n    // 判断方式一\n    if (document.visibilityState == \"hidden\") {\n      console.log(\'用户离开了\')\n    }else {\n      console.log(\'用户回来了\')\n    }\n   \n    // 判断方式二\n    if(document.hidden){\n      console.log(\'用户离开了\')\n    }\n  });\n&lt;/script&gt;\n\n&lt;/html&gt;</code></pre><p><br></p>', NULL, 1, 1, 18, '2023-02-21 11:12:50', '2023-02-24 17:04:53', 1, NULL);
INSERT INTO `article` VALUES (6, 'HTML5事件—visibilitychange页面可见性事件监听', '<p><span style=\"color: rgb(102, 102, 102); background-color: rgb(246, 246, 246); font-size: 14px;\">无意中注意到的这个有意思的api，visibilitychange事件是浏览器新添加的一个事件，当浏览器的某个标签页切换到后台，或从后台切换到前台时就会触发该消息，现在主流的浏览器都支持该消息了，例如Chrome, Firefox, IE10等。虽然这只是一个简单的功能，但这对于采用HTML5开发移动端产品来说是天赐福音，比如用户正在玩游戏时，突然切换到后台去发一条短信或打一个电话，再切换到游戏，那么开发者就需要捕捉对这些突发情形进行处理，当游戏切换到后台时就暂停游戏，从后台切换回来时，又能允许用户继续游戏</span></p><p><br></p><h2 style=\"text-align: start;\"><strong>基本用法</strong></h2><p><span style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 14px;\">通过</span><span style=\"color: rgb(221, 17, 68); background-color: rgb(249, 242, 244);\">document.visibilityState</span><span style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 14px;\">（返回visible | hidden）或者</span><span style=\"color: rgb(221, 17, 68); background-color: rgb(249, 242, 244);\">document.hidden</span><span style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 14px;\">（返回true | false，true为不可见）可以判断当前文档是否可见；提示：w3c官方更推荐使用前者document.visibilityState方式</span></p><p><br></p><pre><code class=\"language-javascript\">&lt;!DOCTYPE html&gt;\n&lt;html lang=\"en\"&gt;\n\n&lt;head&gt;\n  &lt;meta charset=\"UTF-8\"&gt;\n  &lt;meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"&gt;\n  &lt;meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"&gt;\n  &lt;title&gt;Document&lt;/title&gt;\n&lt;/head&gt;\n\n&lt;body&gt;\n\n&lt;/body&gt;\n&lt;script&gt;\n  document.addEventListener(\"visibilitychange\", function () {\n    // 判断方式一\n    if (document.visibilityState == \"hidden\") {\n      console.log(\'用户离开了\')\n    }else {\n      console.log(\'用户回来了\')\n    }\n   \n    // 判断方式二\n    if(document.hidden){\n      console.log(\'用户离开了\')\n    }\n  });\n&lt;/script&gt;\n\n&lt;/html&gt;</code></pre><p><br></p>', 'http://localhost:9999/views?fileName=f9f55e96-d936-4942-867c-93dcce9d5557.png', 1, 1, 26, '2023-02-21 11:12:50', '2023-04-05 17:29:37', 1, NULL);
INSERT INTO `article` VALUES (7, 'B站：程序员瑞哥', '分享Java｜Python｜PHP｜Vue｜H5｜微信小程序｜Uniapp实战项目教学', 'http://localhost:9999/views?fileName=7d891d27-7887-4b03-8508-26b209e08e95.png', 1, 1, 1, '2023-04-05 17:07:48', '2023-04-05 17:11:49', 1, NULL);
INSERT INTO `article` VALUES (8, 'B站：程序员瑞哥', '分享Java｜Python｜PHP｜Vue｜H5｜微信小程序｜Uniapp实战项目教学', 'http://localhost:9999/views?fileName=31389b38-e131-4b03-aa17-c8963d00970d.png', 1, 1, 3, '2023-04-05 17:11:16', '2023-04-05 17:29:29', 1, 1);
INSERT INTO `article` VALUES (9, 'B站：程序员瑞哥', '分享Java｜Python｜PHP｜Vue｜H5｜微信小程序｜Uniapp实战项目教学', 'http://localhost:9999/views?fileName=76ca9feb-e78c-41a8-a630-445ab5dc4b28.jpg', 1, 1, 126, '2023-04-06 09:02:36', '2023-08-07 14:03:21', 1, NULL);
INSERT INTO `article` VALUES (10, 'B站：程序员瑞哥', '分享Java｜Python｜PHP｜Vue｜H5｜微信小程序｜Uniapp实战项目教学', 'http://localhost:9999/views?fileName=5de7f69f-cb72-4c7b-a57c-4bdf8540a42e.jpg', 1, 1, 1004, '2023-04-06 14:07:15', '2023-04-06 14:48:02', 1, 1);
INSERT INTO `article` VALUES (11, 'B站：程序员瑞哥', '分享Java｜Python｜PHP｜Vue｜H5｜微信小程序｜Uniapp实战项目教学', 'http://localhost:9999/views?fileName=c8572994-fe2d-49a0-937a-0dd642a512e4.jpg', 1, 1, 6, '2023-04-06 14:14:44', '2023-08-07 14:03:13', 1, 1);

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '标题',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `status` int(1) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '栏目表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES (1, 'Vue开发', '2022-07-31 13:55:40', '2023-02-21 11:31:24', NULL);
INSERT INTO `category` VALUES (2, 'Spring Cloud开发', '2022-08-01 19:12:20', '2023-02-21 11:34:49', NULL);
INSERT INTO `category` VALUES (3, 'SpringBoot开发', NULL, '2023-02-21 11:34:25', 1);
INSERT INTO `category` VALUES (4, 'SSM开发', NULL, '2023-02-21 11:34:31', 1);
INSERT INTO `category` VALUES (5, 'React开发', '2022-08-04 09:32:53', '2023-02-21 11:34:15', 1);
INSERT INTO `category` VALUES (6, 'JavaScript开发', '2022-08-15 16:21:22', '2023-03-03 15:48:27', 1);
INSERT INTO `category` VALUES (7, 'HTML开发', NULL, '2023-02-24 08:13:34', 1);
INSERT INTO `category` VALUES (8, '测试', '2023-03-17 13:50:58', NULL, 1);
INSERT INTO `category` VALUES (9, '我是最新一条的数据', '2023-03-17 13:54:57', '2023-03-17 18:37:31', 1);
INSERT INTO `category` VALUES (10, 'test', '2023-03-18 19:54:40', '2023-03-18 19:55:55', 1);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '路径',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  `page_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '页面路径',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `status` int(1) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '菜单表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '测试页面Test', '/test', '测试页面Test', '/test', '2023-03-23 14:46:40', '2023-04-06 13:59:00', 0);
INSERT INTO `sys_menu` VALUES (2, '测试页面Test2', '/test2', '测试页面Test2', '/test2', '2023-03-24 20:42:10', '2023-04-06 13:54:43', 0);
INSERT INTO `sys_menu` VALUES (3, '栏目管理', '/categoryManage', '栏目管理', '/categoryManage', '2023-03-24 20:42:13', '2023-03-25 10:21:23', 1);
INSERT INTO `sys_menu` VALUES (4, '文章管理', '/articleManage', '文章管理', '/articleManage', '2023-03-25 10:18:52', NULL, 1);
INSERT INTO `sys_menu` VALUES (5, '用户管理', '/sysUserManage', '用户管理', '/sysUserManage', '2023-03-25 10:19:07', '2023-03-25 10:19:52', 1);
INSERT INTO `sys_menu` VALUES (6, '角色管理', '/sysRoleManage', '角色管理', '/sysRoleManage', '2023-03-25 10:19:23', NULL, 1);
INSERT INTO `sys_menu` VALUES (7, '菜单管理', '/sysMenuManage', '菜单管理', '/sysMenuManage', '2023-03-25 10:19:36', NULL, 1);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '角色名称',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `status` int(1) NULL DEFAULT 1 COMMENT '状态',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '角色描述',
  `role` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '角色标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '普通用户', '2023-03-23 14:28:52', '2023-03-24 23:13:46', 1, '用于普通用户操作的角色', 'ROLE_USER');
INSERT INTO `sys_role` VALUES (2, '超级管理员', '2023-03-24 20:53:25', NULL, 1, '超级管理员', 'ROLE_ADMIN');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_id` int(11) NULL DEFAULT NULL COMMENT '角色id',
  `menu_id` int(11) NULL DEFAULT NULL COMMENT '菜单id',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `status` int(1) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '角色菜单关系表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (18, 2, 2, '2023-03-25 10:21:51', NULL, 1);
INSERT INTO `sys_role_menu` VALUES (19, 2, 1, '2023-03-25 10:21:51', NULL, 1);
INSERT INTO `sys_role_menu` VALUES (20, 2, 3, '2023-03-25 10:21:51', NULL, 1);
INSERT INTO `sys_role_menu` VALUES (21, 2, 4, '2023-03-25 10:21:51', NULL, 1);
INSERT INTO `sys_role_menu` VALUES (22, 2, 5, '2023-03-25 10:21:51', NULL, 1);
INSERT INTO `sys_role_menu` VALUES (23, 2, 6, '2023-03-25 10:21:51', NULL, 1);
INSERT INTO `sys_role_menu` VALUES (24, 2, 7, '2023-03-25 10:21:51', NULL, 1);
INSERT INTO `sys_role_menu` VALUES (25, 1, 1, '2023-03-25 10:22:01', NULL, 1);
INSERT INTO `sys_role_menu` VALUES (26, 1, 2, '2023-03-25 10:22:01', NULL, 1);
INSERT INTO `sys_role_menu` VALUES (27, 1, 4, '2023-03-25 10:22:01', NULL, 1);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '密码',
  `motto` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '座右铭',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '电话',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '地址',
  `avatar_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '头像',
  `role_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '角色类型',
  `views` int(11) NULL DEFAULT 0 COMMENT '文章总浏览量',
  `salt` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '加盐',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `status` int(1) NULL DEFAULT 1 COMMENT '状态 已封禁/已删除/正常',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, '程序员瑞哥', '8f5ed750abe6ef2ac5e05adb085c6e69', '分享Java｜Python｜PHP｜Vue｜H5｜微信小程序｜Uniapp实战项目教学，和大家一起进步~', '231@qq.com', '15587878787', NULL, NULL, 'ROLE_ADMIN', 0, 'F1888D7E9DC1CBD1', '2023-02-21 11:24:56', '2023-04-05 17:57:12', 1);
INSERT INTO `sys_user` VALUES (2, 'testtest', '50b23c07f7b5155d32bf5eadaf56e12a', NULL, '123@qq.com', NULL, NULL, NULL, 'ROLE_USER', 0, '717AB39C997DD0AE', '2023-04-05 17:05:58', '2023-04-05 18:06:43', 1);

SET FOREIGN_KEY_CHECKS = 1;
